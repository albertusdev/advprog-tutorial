package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class ChickenMeat extends Food {
    Food food;

    public ChickenMeat(Food food) {
        // TODO Implement (DONE)
        this.food = food;
    }

    @Override
    public String getDescription() {
        // TODO Implement (DONE)
        return food.getDescription() + ", adding chicken meat";
    }

    @Override
    public double cost() {
        // TODO Implement (DONE)
        return food.cost() + 4.5;
    }
}
