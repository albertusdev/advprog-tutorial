package sorting;

public class Finder {


    /**
     * Some searching algorithm that possibly the slowest algorithm.
     * This algorithm can search a value irregardless of whether the sequence already sorted or not.
     * @param arrOfInt is a sequence of integer.
     * @param searchedValue value that need to be searched inside the sequence.
     * @return -1 if there are no such value inside the sequence, else return searchedValue.
     */

    /*
     * I modified this function to return the index of the searchedValue in the arrOfInt
     * If the return value is the searchedValue:
     *  - It will return wrong output for searchedValue = -1 when -1 is not in the arrOfInt
     * In search algorithm, usually we care about the index. Returning the actual parameter doesn't make snse;
     * If the function itself is used to determine whether a value exist or not in the array, it's better to return boolean.
     */
    public static int slowSearch(int[] arrOfInt, int searchedValue) {
        int returnValue = -1;

        for (int i = 0; i < arrOfInt.length; ++i) {
            int element = arrOfInt[i];
            if (element == searchedValue) {
                returnValue = i;
            }
        }

        return returnValue;
    }

    /**
     * Some searching algorithm that possibly the slowest algorithm.
     * This algorithm can search a value irregardless of whether the sequence already sorted or not.
     * @param arrOfInt is a sequence of integer.
     * @param searchedValue value that need to be searched inside the sequence.
     * @return -1 if there are no such value inside the sequence, else return searchedValue.
     */
    public static int binarySearch(int[] arrOfInt, int searchedValue) {
        int l = 0;
        int r = arrOfInt.length - 1;
        while (l <= r) {
            int mid = (l + r) / 2;
            if (arrOfInt[mid] == searchedValue) {
                return mid;
            } else if (arrOfInt[mid] > searchedValue) {
                r = mid - 1;
            } else {
                l = mid + 1;
            }
        }
        return -1;
    }

}
