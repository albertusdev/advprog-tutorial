colors = ["#424242", "#FAFAFA"]
activeColorIndex = 0

function invertTheme() {
    document.body.style.backgroundColor = colors[activeColorIndex];
    activeColorIndex = (activeColorIndex + 1) % colors.length;
    document.body.style.color = colors[activeColorIndex];
}

setInterval(invertTheme, 5000);
