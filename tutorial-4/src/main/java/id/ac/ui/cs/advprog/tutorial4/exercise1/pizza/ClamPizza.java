package id.ac.ui.cs.advprog.tutorial4.exercise1.pizza;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.PizzaIngredientFactory;

public class ClamPizza extends Pizza {
    public static final String NAME = "Clam Pizza";
    PizzaIngredientFactory ingredientFactory;

    public ClamPizza(PizzaIngredientFactory ingredientFactory) {
        this.ingredientFactory = ingredientFactory;
        setName(NAME);
    }

    public void prepare() {
        System.out.println("Preparing " + NAME);
        dough = ingredientFactory.createDough();
        sauce = ingredientFactory.createSauce();
        cheese = ingredientFactory.createCheese();
        clam = ingredientFactory.createClam();
    }
}
