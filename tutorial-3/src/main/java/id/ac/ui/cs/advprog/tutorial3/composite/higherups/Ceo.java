package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class Ceo extends Employees {
    public static final String ROLE = "CEO";
    public static final double BASE_SALARY = 200000.0;

    public Ceo(String name, double salary) {
        if (salary < BASE_SALARY) throw new IllegalArgumentException();
        // TODO Implement (DONE)
        this.name = name;
        this.salary = salary;
        role = ROLE;
    }

    @Override
    public double getSalary() {
        // TODO Implement (DONE)
        return salary;
    }
}
