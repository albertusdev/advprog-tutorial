package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class BackendProgrammer extends Employees {
    public static final String ROLE = "Back End Programmer";
    public static final double BASE_SALARY = 20000.00;

    // TODO Implement (DONE)
    public BackendProgrammer(String name, double salary) {
        if (salary < BASE_SALARY) throw new IllegalArgumentException();
        this.name = name;
        this.salary = salary;
        role = ROLE;
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
