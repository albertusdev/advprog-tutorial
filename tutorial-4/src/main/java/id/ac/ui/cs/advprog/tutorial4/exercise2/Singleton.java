package id.ac.ui.cs.advprog.tutorial4.exercise2;

public class Singleton {

    // TODO Implement me! (DONE)
    // What's missing in this Singleton declaration?
    private static Singleton instance = new Singleton();

    private Singleton() {
    }


    public static Singleton getInstance() {
        // TODO Implement me! (DONE)
        return instance;
    }
}
